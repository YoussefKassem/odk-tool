import Vue from 'vue'
import Router from 'vue-router'
import Annotator from '@/components/Annotator'
import Canvas from '@/components/Canvas'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'Annotator',
    //   component: Annotator
    // },
    {
      path: '/',
      name: 'Canvas',
      component: Canvas
    }
  ]
})
